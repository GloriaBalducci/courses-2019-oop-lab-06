package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	final ArrayList<Integer> alist = new ArrayList<>();
    	alist.ensureCapacity(1000);
    	for (int number= 1000; number<2000; number++) {
    		alist.add(number);
    	}
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	final LinkedList<Integer> llist = new LinkedList<>();
    	llist.addAll(alist);
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	Integer E = alist.get(0);
    	Integer E1 = alist.get(999);
    	alist.set(0, E1);
    	alist.set(999, E);
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for (final Integer i: alist) {
    		System.out.println ( alist.indexOf(i)+"\t"+ i );
    	}
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    
    	long time = System.nanoTime();
    	for (int number = 0; number < 100000; number++) {
    		alist.add(0, number);
    	}
    	time = System.nanoTime() - time;
    	
    	System.out.println("Inserting 100000 number in alist " + time+ "ns" );
        
        
    	long time1 = System.nanoTime();
    	for (int number =0; number < 100000; number++) {
    		llist.add(0, number);
    	}
    	time1 = System.nanoTime() - time1;
    	
    	System.out.println("Inserting 100000 number in llist " + time1+ "ns");
        
    	for (final Integer i: alist) {
    		System.out.println ( alist.indexOf(i)+"\t"+ i );
    	}
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */

    	long time2 = System.nanoTime();
    	Random nr = new Random();
    	for (int number =0; number < 1000; number++) {
    		
    		int o = nr.nextInt(80000);
    		alist.get(o);
    	}
    	time2 = System.nanoTime() - time2;
    	
    	System.out.println("Reading 1000 number in alist " + time2+ "ns");
        
    	long time3 = System.nanoTime();
    	Random nr1 = new Random();
    	for (int number =0; number < 1000; number++) {
    		
    		int o = nr1.nextInt(80000);
    		llist.get(o);
    	}
    	time3 = System.nanoTime() - time3;
    	
    	System.out.println("Reading 1000 number in alist " + time3+ "ns");
        
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
    	final Map<String,Integer> map = new HashMap < >() ;
    	map . put ("Africa", 1110635000);
    	map.put("Americas",972005000);
    	map.put("Antarctica",0);
    	map.put("Asia",4298723);
    	map.put("Europe",742452000);
    	map.put("oceania",38304000);

        /*
         * 8) Compute the population of the world
         */
    	int somma=0;
    	for ( final Integer s1 : map.values () ) {
    		somma=somma+s1;
    		
    		 }
    	 System . out . println (" Valore : " + somma);

    }
}
